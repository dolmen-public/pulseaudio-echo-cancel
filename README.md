# Pulseaudio & echo cancel

This project helps set up echo cancel in with pulseaudio.

## Install

Run

```copy
sudo curl -fsSL -o /usr/local/bin/pulseaudio_echo_cancel "https://gitlab.com/savadenn-public/pulseaudio-echo-cancel/-/raw/master/configure.sh" && sudo chmod +x /usr/local/bin/pulseaudio_echo_cancel
```

## Use it

```
pulseaudio_echo_cancel
```

and follow the instructions
